﻿using BookShop.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace BookShop.MVC.Controllers
{
    public class BookShopController : Controller
    {
        private readonly IBookShopService _bookShopService;
        public BookShopController(IBookShopService bookShopService) 
        {
            _bookShopService = bookShopService;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Domain.Entities.BookShop bookShop)
        {
            await _bookShopService.Create(bookShop);
            return RedirectToAction(nameof(Create));
        }
    }
}
