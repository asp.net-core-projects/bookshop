﻿using BookShop.Domain.Interfaces;
using BookShop.Infrastructure.Database;
using BookShop.Infrastructure.Repositories;
using BookShop.Infrastructure.Seeders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BookShopDbContext>(options => options.UseSqlServer(
            configuration.GetConnectionString("BookShop")));

            services.AddScoped<BookShopSeeder>();
            services.AddScoped<IBookShopRepository, BookShopRepository>();
        }
    }
}
