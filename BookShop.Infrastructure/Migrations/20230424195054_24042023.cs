﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookShop.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class _24042023 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "About",
                table: "BookShops");

            migrationBuilder.DropColumn(
                name: "UrlCode",
                table: "BookShops");

            migrationBuilder.RenameColumn(
                name: "CreatedAdd",
                table: "BookShops",
                newName: "CreatedAt");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "BookShops",
                newName: "CreatedAdd");

            migrationBuilder.AddColumn<string>(
                name: "About",
                table: "BookShops",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrlCode",
                table: "BookShops",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
