﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookShop.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class _12222add : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ContanctDetails_Street",
                table: "BookShops",
                newName: "ContactDetails_Street");

            migrationBuilder.RenameColumn(
                name: "ContanctDetails_PostalCode",
                table: "BookShops",
                newName: "ContactDetails_PostalCode");

            migrationBuilder.RenameColumn(
                name: "ContanctDetails_NumberPhone",
                table: "BookShops",
                newName: "ContactDetails_NumberPhone");

            migrationBuilder.RenameColumn(
                name: "ContanctDetails_City",
                table: "BookShops",
                newName: "ContactDetails_City");

            migrationBuilder.AddColumn<string>(
                name: "EncodedName",
                table: "BookShops",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EncodedName",
                table: "BookShops");

            migrationBuilder.RenameColumn(
                name: "ContactDetails_Street",
                table: "BookShops",
                newName: "ContanctDetails_Street");

            migrationBuilder.RenameColumn(
                name: "ContactDetails_PostalCode",
                table: "BookShops",
                newName: "ContanctDetails_PostalCode");

            migrationBuilder.RenameColumn(
                name: "ContactDetails_NumberPhone",
                table: "BookShops",
                newName: "ContanctDetails_NumberPhone");

            migrationBuilder.RenameColumn(
                name: "ContactDetails_City",
                table: "BookShops",
                newName: "ContanctDetails_City");
        }
    }
}
