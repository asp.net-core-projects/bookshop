﻿using BookShop.Domain.Interfaces;
using BookShop.Infrastructure.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Infrastructure.Repositories
{
    internal class BookShopRepository : IBookShopRepository
    {
        private readonly BookShopDbContext _dbContext;
        public BookShopRepository(BookShopDbContext dbContext) 
        {
            _dbContext = dbContext;
        }
        public async Task Create(Domain.Entities.BookShop bookShop)
        {
            _dbContext.Add(bookShop);
            await _dbContext.SaveChangesAsync();
        }
    }
}
