﻿namespace BookShop.Application.Services
{
    public interface IBookShopService
    {
        Task Create(Domain.Entities.BookShop bookShop);
    }
}