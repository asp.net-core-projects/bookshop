﻿using BookShop.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Application.Services
{
    public class BookShopService : IBookShopService
    {
        private readonly IBookShopRepository _bookShopRepository;
        public BookShopService(IBookShopRepository bookShopRepository)
        {
            _bookShopRepository = bookShopRepository;
        }
        public async Task Create(Domain.Entities.BookShop bookShop)
        {
            bookShop.CodeUrl();

            await _bookShopRepository.Create(bookShop);
        }
    }
}
